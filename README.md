# Project Aim

To produce a timeline of all lockdown measures implemented for the 47 WHO AFRO countries, and to assess and overcome inaccuracies found in the ACAPS, OXCGRT and ACDC datasets.

# Methods

Government websites for each country in the WHO AFRO region were searched, either manually or using web scraping programs. These websites included the official government website, state house websites, individual websites of the prime minister or president, ministry of health websites, official social media pages, and foreign embassy websites pertaining to the country in question (e.g. the US embassy in Nigeria). If information was missing from official sources, multiple corroborating media reports were used to determine a timeline of measure implementation and to fill in gaps. Source quality is indicated for every measure using the following categorisation:

| Source Type                                                          | Source Quality |
|----------------------------------------------------------------------|----------------|
| Official Government Documents/Press releases from government website | 1              |
| Multiple media reports                                               | 2              |
| Single media report                                                  | 3              |

Data was recorded in a spreadsheet with the following columns and input types:
+	Country (string from list of options)
+	Measure (free text)
+	Implementation Date (string from list of options)
+	OxCGRT-Binary (binary)
+	OxCGRT-Comment (free text)
+	ACAPS-Binary (binary)
+	ACAPS-Comment (free text)
+	ACDC-Binary (binary)
+	ACDC-Comment (free text)
+	Verification Source Quality (integer 1-3)
+	Category (string from list of options)
+	Subcategory (string from list of options)
+	Easing/Strengthening Lockdown (string from list of options)

The timeline of measures recorded was then compared to three existing  third-party datasets (ACDC, ACAPS and OxCGRT). Binary flags indicate whether the dataset recorded a measure on the same date as determined by our methodology, plus or minus one day.  The comment field explains the reasons for a score of 1 or 0 for the binary flag for each dataset comparison. Detailed results of the comparison are available in the [formal writeup](https://git.ecdf.ed.ac.uk/epigroup/covid-19/lockdown_timelines/blob/master/Formal_Writeup.docx) and as a standalone [accuracy review](https://git.ecdf.ed.ac.uk/epigroup/covid-19/lockdown_timelines/blob/master/Accuracy_Comparison.md) for a brief overview.


Categories and Subcategories of measures were derived from those used by the [ACAPS dataset](https://www.acaps.org/covid19-government-measures-dataset#:~:text=The%20%23COVID19%20Government%20Measures%20Dataset,available%20falls%20into%20five%20categories%3A&text=Social%20and%20economic%20measures) and manually assigned to each measure. A more detailed methodology is available, also in the [formal writeup](https://git.ecdf.ed.ac.uk/epigroup/covid-19/lockdown_timelines/blob/master/Formal_Writeup.docx) of the dataset.

# Strictness Scores

Using similar thinking to the [OxCGRT](https://www.bsg.ox.ac.uk/research/research-projects/coronavirus-government-response-tracker), custom strictness scales were devised for each of the 14 subcategories:

| Subcategory                                                  | Strictness Level                                                                                 | Score |
|--------------------------------------------------------------|--------------------------------------------------------------------------------------------------|-------|
| Border closure                                               | Borders open as normal                                                                           | 0     |
|  | Borders shut to some targeted countries, but not all affected countries                          | 1     |
|  | Borders shut to all affected countries or a large list of countries                              | 2     |
|  | Borders closed completely (except for repatriation, cargo and aid)                               | 3     |
| Border health checks                                         | No border health checks                                                                          | 0     |
|  | Basic test (e.g. temperature) for those from affected countries                                  | 1     |
|  | Basic (e.g. temperature) for all arrivals                                                        | 2     |
|  | Advanced test for those from specific countries/Health document required from specific countries | 3     |
|  | Advanced test for all arrivals/Health document required from all arrivals                        | 4     |
| Closure of businesses and public services                    | No closures                                                                                      | 0     |
|  | Some businesses/services limited in hours                                                        | 1     |
|  | Entertainment based businesses/services closed                                                   | 2     |
|  | Entertainment based businesses/services closed, others limited in hours                          | 3     |
|  | Entertainment and Hospitality closed                                                             | 4     |
|  | Entertainment and Hospitality closed, others limited in hours                                    | 5     |
|  | All non-essential businesses closed                                                              | 6     |
| Curfews                                                      | No specific curfew present                                                                       | 0     |
|  | Targeted curfew for some areas after 8pm                                                         | 1     |
|  | Targeted curfew for some areas before 8pm                                                        | 2     |
|  | Nationwide curfew after 8pm                                                                      | 3     |
|  | Nationwide curfew before 8pm                                                                     | 4     |
| Domestic travel restrictions                                 | Able to travel freely throughout the whole country                                               | 0     |
|  | Very long distance travel (e.g. domestic flights) suspended                                      | 1     |
|  | Long distance travel (e.g. between provinces) suspended                                          | 2     |
|  | Medium distance travel (outside your city) suspended                                             | 3     |
|  | Short distance travel (the next town/village) suspended                                          | 4     |
| Emergency administrative structures activated or established | None                                                                                             | 0     |
|  | In place                                                                                         | 1     |
| Full lockdown                                                | No lockdown                                                                                      | 0     |
|  | Citizens required to stay at home nationwide                                                     | 1     |
|  | Partial lockdown only in targeted areas of the country                                           | 2     |
| Isolation and quarantine policies                            | No policy                                                                                        | 0     |
|  | Travellers from affected countries                                                               | 1     |
|  | All travellers                                                                                   | 2     |
|  | Any possible exposure                                                                            | 3     |
| Limit public gatherings                                      | No limit on public gatherings                                                                    | 0     |
|  | Gatherings over 1000 suspended                                                                   | 1     |
|  | Gatherings over 100 suspended                                                                    | 2     |
|  | Non-religious gatherings over 50 suspended                                                       | 3     |
|  | Gatherings over 50 suspended                                                                     | 4     |
|  | Non-religious gatherings over 10 suspended                                                       | 5     |
|  | Gatherings over 10 suspended                                                                     | 6     |
|  | Restriction on gatherings less than 10                                                           | 7     |
| Partial lockdown                                             | No partial lockdown                                                                              | 0     |
|  | Relaxed/Non-strict nationwide lockdown                                                           | 1     |
|  | Full lockdown only in targeted areas of the country                                              | 2     |
| Public health recommendations                                | None                                                                                             | 0     |
|  | Recommendations in place                                                                         | 1     |
| Requirement to wear protective gear in public                | No requirement                                                                                   | 0     |
|  | Requirement for specific places only                                                             | 1     |
|  | Requirement for most public places                                                               | 2     |
| Schools closure                                              | All schools open                                                                                 | 0     |
|  | Some levels of school closed                                                                     | 1     |
|  | All levels of school closed with exceptions for examinations                                     | 2     |
|  | All levels of school closed                                                                      | 3     |
| Surveillance and testing                                     | No surveillance and monitoring                                                                   | 0     |
|  | Targeted/infrequent population surveillance                                                      | 1     |
|  | Mass population surveillance                                                                     | 2     |
|  | Mass population testing                                                                          | 3     |


A normalised strictness score for plotting has been calculated by dividing all the strictness scores for each subcategory by the maximum possible value for that category. These normalised strictness values can be plotted comparatively:

![image of algeria](/images/algeria_plot.png)

# Indexes

Based on these normalised strictness scores, two indices were calculated by following OxCGRT methods.

**Government Response Index**
* This the average normalised strictness values of all 14 subcategories of measures.

**Stringency Index**
* This the average normalised strictness values of 12 subcategories of measures, excluding the governance and socio-economic measures and surveillance and testing from public health measures.

# Dataset Versions

## Epigroup_Lockdown_Timeline_Dataset.xlsx:
+	This is all the recorded measures for all the countries, with comparison to the three third party datasets.

## Epigroup_Lockdown_Timeline_Dataset_withStrictness.csv:
+	This is the same data from the first file, but with the strictness values added only for the dates when measures were implemented/the strictness values changed.
+	There is an added column for the raw strictness score of each category, and an added column for the normalised strictness score of each category (denoted by 'n_{categoryName}'.
+	Strictness scores are normalised by dividing the current value by the max possible value for that category, so border closure : 2 would be 2/3 or 0.666.

## Epigroup_Lockdown_Timeline_Dataset_strictnessTimeseries.csv:
+	This is only the strictness scores, normalised strictness scores and indexes for a country as a timeseries, with one row for every day between the first and last recorded measure.
